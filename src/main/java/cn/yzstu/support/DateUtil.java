package cn.yzstu.support;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
    public final static String DATE_A = "yyyy-MM-dd";
    public final static String DATE_B = "yyyy-MM-dd HH:mm:ss";
    public final static String DATE_C = "yyyyMMddHHmmss";
    public final static String DATE_D = "yyyyMMdd-HHmmss-SS";
    public final static String DATE_E = "M月d日";
    public final static String DATE_F = "MM-dd";
    public final static String DATE_G = "yyyyMMddHHmmss";


    // 普通的当前时间转字符串方法，格式为yyyy-MM-dd
    public static String getDate() {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_A);
        return sdf.format(new Date());
    }

    public static String getDateTime() {
        Date date = new Date();
        String datestr;
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_B);
        datestr = sdf.format(date);
        return datestr;
    }
}
