package cn.yzstu.support;
/**
 *@描述 用于判断当前系统
 *@参数
 *@返回值
 *@创建人  Baldwin
 *@创建时间  2020/4/4
 *@修改人和其它信息
 */
public class SystemUtil {

    /**
     * 判断系统时win还是linux
     * @return
     */
    public static boolean isLinux(){
        String name = System.getProperty("os.name");
        if(name.toLowerCase().startsWith("win"))
            return false;
        else
            return true;
    }
}
