package cn.yzstu.tt;

import cn.yzstu.annotation.YzLogWrite;
import cn.yzstu.core.StartWork;

public class Demo {

    //因为测试用的main函数是static，所以此时将age设置为static
    @YzLogWrite(value = 18,msgPrefix = "记录Baldwin的年龄：")
    static int age;

    public static void main(String[] args) {
        StartWork.doWork();
        System.out.println(age);
    }

}
