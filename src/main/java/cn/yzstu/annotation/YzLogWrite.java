package cn.yzstu.annotation;

import java.lang.annotation.*;

//作用与域或者本地变量
@Target({ElementType.FIELD})
//运行时生效
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface YzLogWrite {

    //需要注解的值
    int value() default -1;

    //默认是Linux系统，默认记录文件夹如下
    String path() default "";

    //文件名
    String fileName() default "";

    //内容
    String msgPrefix() default "";
}
