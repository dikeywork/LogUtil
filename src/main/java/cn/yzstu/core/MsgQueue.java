package cn.yzstu.core;

import cn.yzstu.beans.LogMsg;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedDeque;

public class MsgQueue {

    private static Queue<LogMsg> queue = new ConcurrentLinkedDeque<>();

    //消息入列
    public static boolean push(LogMsg logMsg){
        return queue.offer(logMsg);
    }

    //消息出列
    public static LogMsg poll(){
        return queue.poll();
    }

    //消息队列是否已经处理完毕，处理完毕返回true
    public static boolean isFinash(){
        return !queue.isEmpty();
    }
}
