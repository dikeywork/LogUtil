package cn.yzstu.core;

import java.util.ResourceBundle;

public class ConfigurationUtil {
    private static Object lock = new Object();
    private static ConfigurationUtil config = null;
    private static ResourceBundle rb = null;

    private ConfigurationUtil(String filename) {
        rb = ResourceBundle.getBundle(filename);
    }

    public static ConfigurationUtil getInstance(String filename) {
        synchronized (lock) {
            if (null == config) {
                config = new ConfigurationUtil(filename);
            }
        }
        return (config);
    }

    public String getValue(String key) {
        String ret = "";
        if (rb.containsKey(key)) {
            ret = rb.getString(key);
        }
        return ret;
    }
}
